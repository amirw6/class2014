// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
  apiKey: "AIzaSyCo7TpxrHhAa3vC5k1KRycaLYL80pPLf84",
  authDomain: "tech-98bb4.firebaseapp.com",
  databaseURL: "https://tech-98bb4.firebaseio.com",
  projectId: "tech-98bb4",
  storageBucket: "tech-98bb4.appspot.com",
  messagingSenderId: "875500108976",
  appId: "1:875500108976:web:42cad20af219f1964c4f75",
  measurementId: "G-PX5XX8M62R"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
