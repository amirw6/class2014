import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {
  panelOpenState = false;
  books:any;
  books$:Observable<any>;
  userId:string;

  deleteBook(id:string){
    this.BooksService.DeleteBook(id,this.userId);
  }

  constructor(private BooksService:BooksService,public authService:AuthService) { 

  }

  ngOnInit() {
    /*
    this.books = this.BooksService.getBooks().subscribe(
      (books) => this.books = books
    );
  }
  
    this.books$= this.BooksService.getBooks();
    
    //this.BooksService.addBooks();
*/
      this.authService.user.subscribe(
        user=>{
          this.userId = user.uid;
          this.books$ = this.BooksService.getBooks(this.userId);
        }
      )

  }

}
