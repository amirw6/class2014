export interface Weather {

    name:String,
    country:String,
    image:String,
    discription:String,
    temperature:number,
    lat?:number,
    lon?:number


}
