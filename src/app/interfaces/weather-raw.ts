export interface WeatherRaw {
    
  weather:[
    {
        discription:String,
      icon:String,
    }
  ];
  
  main:{
    temp:number,
  };

  sys:{
      country:String,
  }
  coord:{
      lon:number,
      lat:number
  }
  name:String

}

