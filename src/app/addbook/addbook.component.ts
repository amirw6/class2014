import { AuthService } from './../auth.service';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-addbook',
  templateUrl: './addbook.component.html',
  styleUrls: ['./addbook.component.css']
})
export class AddbookComponent implements OnInit {

  title:string;
  author:string;
  id:string;
  isEdit:boolean = false;
  buttonText:string = "Add Book";
  userId:string;

  constructor(private router:Router, private bookservice:BooksService,
     private route:ActivatedRoute,
     public authService:AuthService) {

   }
   onSubmit(){

    if(this.isEdit){
      this.bookservice.updateBook(this.userId,this.id,this.title,this.author);
      
    }
    else{
      this.bookservice.addBook(this.userId,this.title,this.author);
    }
    this.router.navigate(['/books']);
  }

  ngOnInit() {
    
    this.id= this.route.snapshot.params.id;
    this.authService.user.subscribe(
      user => {
        this.userId=user.uid;
    if(this.id){
      this.isEdit = true;
      console.log(this.isEdit);
      console.log(this.id);
      this.buttonText = "update book";
      this.bookservice.getBook(this.id).subscribe(
        book => {
          this.title =  book.data().title;
          this.author =   book.data().author;
         
        }
      )
    }
  })
}
}

  
